
de {

    def featureBranch = ~/^feature\/.*$/
    def hotfixBranch = ~/^hotfix\/.*$/
    def stableBranch = ~/^stable$/

    // TOOD: fb: add more
    def mvnProjects = 'cv:lang'
    def rootPath = 'code/cv-root'
    def mavenSettingsPath = '/var/lib/jenkins/assets/maven/settings.xml'
    def branch = env.BRANCH_NAME

    echo "Running branch: ${branch}"

    if((branch =~ featureBranch).matches()) {

        stage('GIT: Check conflicts') {
            echo 'checking for merge conflicts'

            checkout scm

            sh 'git config user.email "jenkins@localhost"'
            sh 'git config user.name "Jenkins"'

            def mergeBranch = 'stable'

            def exitCode = sh (
                script: "git merge --no-commit origin/${mergeBranch}",
                returnStatus: true
            )

            if(exitCode == 0) {
                currentBuild.result = 'SUCCESS'
            } else if(exitCode == 1) {
                currentBuild.result = 'FAILURE'
                echo "branch \"${branch}\" could not be merged into \"${mergeBranch}\" without conflicts."
                sh 'git merge --abort'
                notifyFailed()
                error('merge failed')
            } else {
                currentBuild.result = 'UNSTABLE'
                echo "exit code: ${exitCode}"
                echo "some fatal error has occured, please see logs for details"
                notifyFailed()
                error('merge error')
            }
        }
    }

    stage('Checkstyle') {
        echo 'checking code styling'

        withMaven(maven: 'maven',
                  mavenLocalRepo: '',
                  mavenOpts: '',
                  mavenSettingsFilePath: mavenSettingsPath) {
            def exitCode = sh (
                script: "cd '${rootPath}' && mvn clean checkstyle::check",
                returnStatus: true
            )
            processExitCode(exitCode: exitCode, errorMessage: "checkstyle errors found")
        }
    }

    stage('Compilation') {
        echo 'compiling'

        withMaven(maven: 'maven',
                  mavenLocalRepo: '',
                  mavenOpts: '',
                  mavenSettingsFilePath: mavenSettingsPath) {
            def exitCode = sh (
                script: "cd '${rootPath}' && mvn -pl '${mvnProjects}' compile",
                returnStatus: true
            )
            processExitCode(exitCode: exitCode, errorMessage: "compilation failed")
        }
    }

    stage('Unit testing') {
        echo 'unit testing'

        withMaven(maven: 'maven',
                  mavenLocalRepo: '',
                  mavenOpts: '',
                  mavenSettingsFilePath: mavenSettingsPath) {
            def exitCode = sh (
                    script: "cd '${rootPath}' && mvn -pl '${mvnProjects}' test",
                    returnStatus: true
            )
            processExitCode(exitCode: exitCode, errorMessage: "some unit tests failed")
            // TODO: add unit test results displaying
        }
    }

    stage('Packaging') {
        echo 'packaging'

        withMaven(maven: 'maven',
                  mavenLocalRepo: '',
                  mavenOpts: '',
                  mavenSettingsFilePath: mavenSettingsPath) {
            def exitCode = sh (
                script: "cd '${rootPath}' && mvn -pl '${mvnProjects}' package -DskipTests -Dmaven.main.skip",
                returnStatus: true
            )
            processExitCode(exitCode: exitCode, errorMessage: "packaging failed")
        }
    }

    if((branch =~ hotfixBranch).matches() || (branch =~ stableBranch).matches()) {

        stage('Prepare env for integration testing') {
            echo 'starting env preparation for integration testing'
            sleep 10
        }

        stage('Integration testing') {
            echo 'starting integration testing'
            sleep 10
        }

        stage('Triggerring metrics') {
            build job: 'metrics'
        }
    }

    notifySuccessful()

}

def notifySuccessful() {
    echo 'notify Successful'
    slackSend (color: '#00FF00', message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
  //
  // emailext (
  //     subject: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
  //     body: """<p>SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
  //       <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>""",
  //     recipientProviders: [[$class: 'DevelopersRecipientProvider']]
  //   )
}

def notifyFailed() {
    echo 'notify Failed'
    slackSend (color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
  //
  // emailext (
  //     subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
  //     body: """<p>FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
  //       <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>""",
  //     recipientProviders: [[$class: 'DevelopersRecipientProvider']]
  //   )
}

def processExitCode(args) {
    if(args.exitCode == 0) {
        currentBuild.result = 'SUCCESS'
    } else {
        currentBuild.result = 'FAILURE'
        echo "exit code: ${args.exitCode}"
        echo "${args.errorMessage}"
        notifyFailed()
        error("Error: ${args.errorMessage}")
    }
}

